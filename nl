#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "getch.h"
#include "getch.c"
#include "string.h"
#include "string.c"

#include <CUnit/CUnit.h>
#include <CUnit/Console.h>
#include <CUnit/Basic.h>

void test1();
int info(char* string1, char* string2);

int info(char* string1, char* string2)
{ 
int minustest1 = 0;
int minustest2 = 0;
int resultTests = 0;
char key[68] = "QWRTYUIOP{}ASDFGHJKL:\"ZXC,VBNM<>?qwrtyuiop[]\\asdfghjkl;\'zxcvbnm/";
char key2[4] = ".eE";

char* checkString1 = strpbrk(string1, key2);
char* checkString2 = strpbrk(string2, key2);
char* checkString3 = strpbrk(string1, key);
char* checkString4 = strpbrk(string2, key);

for (int i = 0; i<strlen(string1); i++)
{
if (string1[i] == '-')
{
minustest1++;
}
}
for (int i = 0; i<strlen(string2); i++)
{
if (string2[i] == '-')
{
minustest2++;
}
}
if ((minustest1>1) || (minustest2>1))
{
resultTests = 3;
}
else
{
if ((checkString3 == NULL) && (checkString4 == NULL) 
&& (checkString1 == NULL) && (checkString2 == NULL))
{
resultTests = 1;
}
else
{
if ((checkString3 == NULL) && (checkString4 == NULL) 
&& (checkString1 != NULL) && (checkString2 != NULL))
{
resultTests = 2;
}
else
{ 
resultTests = 3;
}
} 
}
return resultTests;
}

void test1()
{
char string1Test[4] = "123";
char string2Test[3] = "12";
int resultTestsTest = 1;
CU_ASSERT_EQUAL(info(string1Test, string2Test), resultTestsTest);
char string1Test2[6] = "123,2";
char string2Test2[5] = "12,2";
resultTestsTest = 2;
CU_ASSERT_EQUAL(info(string1Test2, string2Test2), resultTestsTest);
char string1Test3[6] = "1dsds";
char string2Test3[6] = "sdfrf";
resultTestsTest = 3;
CU_ASSERT_EQUAL(info(string1Test3, string2Test3), resultTestsTest);
} 

int main()
{
CU_pSuite suite;
CU_initialize_registry();
suite = CU_add_suite("test_suite", NULL, NULL);
CU_ADD_TEST(suite, test1);
CU_basic_run_tests();
CU_cleanup_registry();

printf("Enter your lines\n");
char* string1 = NULL;
char* string2 = NULL;
int resultTests = 0;
string1 = inputString(string1, 32, 122);
string2 = inputString(string2, 32, 122);

resultTests = info(string1, string2);

if(resultTests == 1)
{
int checkString1 = atoi(string1);
int checkString2 = atoi(string2);
int tmp = checkString1 + checkString2;
printf("The sum of the integers is %d\n", tmp);
}

if(resultTests == 2)
{
double checkString3 = atof(string1);
double checkString4 = atof(string2);
double tmp = checkString3/checkString4;
printf("A/B=%f\n", tmp);
}

if(resultTests == 3)
{
strcat(string1, string2);
printf("What is it \"%s\"?\n", string1);
}

return 0;
})׬